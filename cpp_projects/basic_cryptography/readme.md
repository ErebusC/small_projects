Basic Cryptography
==================

Function: 
---------

Moves every character up three spaces in the ASCII table to "encrypt" the sentence.

Description/Background information
----------------------------------

I created this program because I was really interested on encrypting messages, I wanted to try and create a program that you good give some kind of message and it would give you an "encrypted version". I ended up using a cipher of plus 3 for this, this actually taught me some interesting things that I didn't think about. I hadn't previously worked a whole lot with ASCII or representing numbers to the ASCII table so when working on this project and the way I went about it, I had to consider spaces and how capitalised characters were represented differently.